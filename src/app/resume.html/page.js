import CssBaseline from '@mui/material/CssBaseline'
import Container from '@mui/material/Container'
import Box from '@mui/material/Box'

import './mordred.css'

export default function Resume() {
  return (
    <>
      <CssBaseline />

      <Container maxWidth="xl">
        <div className="jumbotron">
          <h1>Monty Taylor</h1>
          <ul className="lead">
            <li>+1 206 471 3749 </li>
            <li>mordred@inaugust.com </li>
            <li>
              <a href="https://matrix.to/#/@mordred:waterwanders.com">
                @mordred:waterwanders.com
              </a>{' '}
            </li>
            <li>
              <a href="https://keybase.io/mordred">keybase.io/mordred</a>{' '}
            </li>
          </ul>
        </div>

        <div className="techskills">
          SRE, Open Source Executive, and Free Software Hacker. Expert in Cloud
          Computing, Large Scale Systems, CI/CD.
        </div>

        <h3 className="resumesection">
          <span className="titletitle">Links and Media</span>
        </h3>

        <div className="techskills">
          <a href="https://en.wikipedia.org/wiki/Monty_Taylor">
            Wikipedia: Monty_Taylor
          </a>
        </div>
        <div className="techskills">
          <a href="http://www.businessinsider.com/most-important-people-in-cloud-computing-2014-4#no-22-hps-monty-taylor-changing-how-big-companies-build-clouds-18">
            Infoworld: The 39 Most Important People in Cloud Computing
          </a>
        </div>
        <div className="techskills">
          <a href="https://www.wired.com/2013/04/new-hackers-taylor">
            Wired: Why Open Source Software is like Burning Man (only better)
          </a>
        </div>
        <div className="techskills">
          <a href="https://medium.com/@Jay_Jamison/that-kind-of-blew-my-mind-aka-missing-monty-taylor-dfba9594310f#.wu7a5zyu8">
            Jay Jamison: The kind of blew my mind
          </a>
        </div>

        <h3 className="resumesection">
          <span className="titletitle">Work History</span>
        </h3>

        <div className="techskills">
          <div className="job">
            <p>Oracle</p>
            <p className="jobtitle">Architect, OCI Substrate: Seattle, WA</p>
            <p className="jobskillslist">2020-present</p>
            <ul>
              <li>
                Architect responsible for the Substrate, the underlying
                infrastructure on top of which OCI is deployed.
              </li>
              <li>
                Designed and implemented both processes and systems for managing
                the intersection of the capacity needed by service teams and the
                hardware BOM ordered for standing up new regions.
              </li>
              <li>
                Drove multiple programs related to shrinking a cloud designed
                for hyperscale into ever smaller physical footprints.
              </li>
              <li>
                Coordinated activities on behalf of the Compute Organization
                related to modernizing deployment automation.
              </li>
            </ul>
          </div>

          <h3 className="subsection">
            <span className="titletitle">OpenStack</span>

            <span className="titledate">2010-2020</span>
          </h3>

          <div className="summary">
            By the time I left in 2020, OpenStack was one of the fastest-growing
            open-source communities in the world. It was forecast to have a
            global market revenue of $5.63 billion USD in 2020 and $6.73 billion
            USD in 2021. As one of the project founders I wore enumerable hats,
            including but not limited to working in leadership positions for
            several of the corporations involved in the 501(c)6 non-profit
            Foundation that acts as caretaker of the project, in technical
            leadership positions within the open source project, and on the
            Board of Directors of the Foundation.
          </div>

          <div className="job">
            <p>Red Hat</p>
            <p className="jobtitle">
              Member of Technical Staff, Office of the CTO: Remote
            </p>
            <p className="jobskillslist">2016-2020</p>
            <ul>
              <li>
                Drove strategy related to CI/CD for the Ansible ecosystem.
              </li>
              <li>Led development team focused on developing Zuul.</li>
              <li>Participated in the SRE responsibilities for OpenDev.</li>
              <li>
                Coordinated with Ansible organization on product related efforts
                around Zuul.
              </li>
            </ul>
          </div>

          <div className="job">
            <p>IBM</p>
            <p className="jobtitle">
              Distinguished Engineer / Director of Engineering
            </p>
            <p className="jobskillslist">2015-2016</p>
            <ul>
              <li>
                Led OpenStack Innovation team focused on CI/CD and scaling of
                OpenStack for Public Cloud.
              </li>
              <li>Led development team focused on developing Zuul.</li>
              <li>Participated in the SRE responsibilities for OpenDev.</li>
            </ul>
          </div>

          <div className="job">
            <p>Hewlett-Packard</p>
            <p className="jobtitle">
              Distinguished Technologist / Director of Engineering
            </p>
            <p className="jobskillslist">2011-2015</p>
            <ul>
              <li>
                Staffed and led the OpenDev SRE team. Grew community consensus
                of the need for broader staffing and succeeded in achieving
                divestiture of the team from being a single-company team.
              </li>
              <li>
                Led teams focused on CI and CD of OpenStack for both Public and
                Private cloud products
              </li>
              <li>
                Managed coopetition between hundreds of competing and
                collaborating companies, both directly in the form of influence
                and interpersonal relationships, and through implementation of
                systems to ensure level playing fields.
              </li>
              <li>
                Grew two separate teams in parallel, one internal and one
                external. Each went from 2 to ~50 over a two year period.
              </li>
              <li>
                Drove partnerships and customer engagements related to OpenStack
                externally. Represented OpenStack internally to engineering
                organizations across Business Units.
              </li>
              <li>
                Staffed for and achieved technical leadership and recognition
                for HP in OpenStack. Based on C-Suite requests housed over half
                of the OpenStack Technical Committee and several Projet Team
                Leads within my team. Drove HP to be the top overall contributor
                to OpenStack.
              </li>
              <li>
                Oversaw the creation of TripleO, which was the basis of HP
                Helion and is now the foundation of Red Hat's RHSOP OpenStack
                product line.
              </li>
              <li>Oversaw the creation of Ironic and Bifrost.</li>
              <li>Oversaw the creation of Zuul.</li>
              <li>Oversaw the stabalization of Keystone.</li>
            </ul>
          </div>

          <div className="job">
            <p>Rackspace Cloud</p>
            <p className="jobtitle">System Architect: Remote</p>
            <p className="jobskillslist">2010-2011</p>
            <ul>
              <li>
                Helped start the OpenStack project (see creation line on
                <a href="https://launchpad.net/openstack">
                  https://launchpad.net/openstack
                </a>
                , it's a fun bit of history).
              </li>
              <li>
                Designed and implemented the development process and the systems
                to support it. Instituted the concept of "gated commits" where
                code only landed if if was code reviewed and all automated tests
                passed. Under this system, from Day Zero of the project, no
                developer had, or has ever had, direct push access.
              </li>
              <li>
                Founded the SRE function responsible for running all of the
                tooling and systems needed to support the OpenStack project.
              </li>
              <li>
                Laid ground work for scaling the developer ecosystem from ~40
                developers to this high water mark of 2500 in any given six
                month period.
              </li>
              <li>
                Coordinated with varying and competing interests from teams at
                Rackspace, NASA and Canonical.
              </li>
            </ul>
          </div>

          <h3 className="subsection">
            <span className="titletitle">Open Source</span>
          </h3>

          <div className="summary">
            As Open Source has taken over the industry, work directly on
            constituent projects and associated non-profit organizations has
            become paramount.
          </div>

          <div className="job">
            <p>OpenStack Foundation Board of Directors</p>
            <p className="jobtitle">Individual Member</p>
            <p className="jobskillslist">2012-2016,2018-2020</p>
            <ul>
              <li>
                Oversaw business, legal, product and branding concerns
                representing the Individual Members of the Foundation.
              </li>
            </ul>
          </div>

          <div className="job">
            <p>
              <a href="https://opendev.org">OpenDev</a>
            </p>
            <p className="jobtitle">Founder, SRE, Core Team Member</p>
            <p className="jobskillslist">2010-present</p>
            <ul>
              <li>
                Founded OpenDev, which started as the OpenStack Infrastructure
                team. Responsible for development, operation and scaling of all
                of the support systems for the development of the OpenStack
                project.
              </li>
              <li>
                Provided initial product management. Expanded that role to be a
                collaboratively driven group product vision.
              </li>
              <li>
                Built and operated world's largest Open Source CI
                infrastructure, supporting 2500 developers world wide, landing
                10k changes per month and consuming approximately 20k cloud
                servers per day.
              </li>
              <li>
                Oversaw the creation of Zuul, Nodepool, Storyboard, git-review,
                pbr, elastic-recheck and countless other systems
              </li>
              <li>
                Wrote shade library, which became openstacksdk, to drive
                multi-cloud interoperability and orchestration issues found in
                production.
              </li>
              <li>
                Ansible core contributor and owner of OpenStack and Puppet
                modules
              </li>
            </ul>
          </div>

          <div className="job">
            <p>
              <a href="https://zuul-ci.org">Zuul</a>
            </p>
            <p className="jobtitle">Co-founder, Maintainer</p>
            <p className="jobskillslist">2012-present</p>
            <ul>
              <li>Co-founder of Zuul, a project gating system.</li>
              <li>
                Oversaw product development and project growth, including
                corporate resource sponsorship.
              </li>
              <li>
                Provided technical marketing worldwide via conference
                interactions.
              </li>
            </ul>
          </div>

          <div className="job">
            <p>
              <a href="https://www.python.org/psf/members/#fellows">
                Python Software Foundation
              </a>
            </p>
            <p className="jobtitle">Fellow</p>
            <p className="jobskillslist">2012-present</p>
          </div>

          <div className="job">
            <p>OpenStack Technical Committee Member</p>
            <p className="jobskillslist">2012-2017</p>
            <ul>
              <li>Oversaw technical governance of the OpenStack project.</li>
              <li>
                Grew project and organization from 50 to 2500 contributors.
              </li>
            </ul>
          </div>

          <div className="job">
            <p>OpenStack Project Policy Board Member</p>
            <p className="jobskillslist">2011-2012</p>
            <ul>
              <li>
                Oversaw overall governance of OpenStack in the days before the
                Foundation was established.
              </li>
            </ul>
          </div>

          <h3 className="subsection">
            <span className="titletitle">Non-profit</span>
          </h3>

          <div className="job">
            <p>
              <a href="https://nolakittenfoster.org/">Nola Kitten Foster</a>
            </p>
            <p className="jobtitle">
              Founder, Vice-President, Secretary: New Orleans, LA
            </p>
            <p className="jobskillslist">2020-present</p>
            <ul>
              <li>
                Founded a non-profit New Orleans based foster nursery for young
                kittens. Work with local animal shelters as well as TNR
                programs.
              </li>
            </ul>
          </div>

          <div className="job">
            <p>NYU</p>
            <p className="jobtitle">Adjunct Professor, ITP: New York, NY</p>
            <p className="jobskillslist">2016</p>
            <ul>
              <li>
                Taught course on "Lighting Without the Board" - focusing on
                custom programmatic control of lighting and overview of lighting
                design.
              </li>
              <li>Guest lectures on Privacy in the Digitial Age.</li>
            </ul>
          </div>

          <div className="job">
            <p>Camp Pot Luck</p>
            <p className="jobtitle">Founder, General Manager</p>
            <p className="jobskillslist">2010-2014</p>
            <ul>
              <li>
                Started and ran a Burning Man Theme Camp focused on feeding
                people.
              </li>
              <li>Provided dinners for roughly 150 people per day.</li>
              <li>
                Managed budget and infrastructure needs, such as fresh and grey
                water, electrical, structural, kitchen creation, plumbing and
                camp membership.
              </li>
            </ul>
          </div>

          <div className="job">
            <p>The Satori Group</p>
            <p className="jobtitle">Company Member</p>
            <p className="jobskillslist">2009-2011</p>
            <ul>
              <li>
                Member of collaborative theatre ensemble focused on
                consensus-oriented new work generation. If this seems irrelevant
                on a tech resume, please note the time period, and then the time
                period where we founded OpenStack on a system of leaderless
                group consensus.
              </li>
              <li>
                Designed and implemented electrical refit of company space to
                create a general purpose performance venue.
              </li>
            </ul>
          </div>

          <h3 className="subsection">
            <span className="titletitle">The MySQL Years</span>

            <span className="titledate">2005-2010</span>
          </h3>

          <div className="summary">
            Working at MySQL was the first (but not last) time I was part of the
            largest Open Source acquisition in history. As the world's most
            popular Open Source database, our clients included basically
            everyone. After the Sun acquisition we were allowed to fork the
            primary product and work on re-thinking the internals, because Sun.
          </div>

          <div className="job">
            <p>Rackspace</p>
            <p className="jobtitle">System Architect: Remote</p>
            <p className="jobskillslist">
              C++, Python, Drizzle, Linux, OSX, Solaris
            </p>
            <ul>
              <li>
                Core developer on{' '}
                <a href="http://launchpad.net/drizzle">Drizzle</a>: a modern
                fork of MySQL for the Cloud
              </li>
              <li>Wrangled Drizzle build and plugin sub-systems.</li>
              <li>
                Managed and owned Drizzle coding standards and C++
                standardization.
              </li>
              <li>
                Authored{' '}
                <a href="http://launchpad.net/pandora-build">pandora-build</a> -
                A set of simple and robust autotools macros.
              </li>
              <li>Worked on Bazaar and Rackspace Cloud plugins for Hudson</li>
            </ul>
          </div>

          <div className="job">
            <p>Sun Microsystems</p>
            <p className="jobtitle">Staff Engineer: Remote (Seattle, WA)</p>
            <p className="jobskillslist">
              C++, Drizzle, MySQL, Linux, OSX, Solaris
            </p>
            <ul>
              <li>
                Core developer on{' '}
                <a href="http://launchpad.net/drizzle">Drizzle</a>: a modern
                fork of MySQL for the Cloud
              </li>
              <li>Wrangled Drizzle build and plugin sub-systems.</li>
              <li>
                Managed and owned Drizzle coding standards and C++
                standardization.
              </li>
              <li>Drizzle "Captain" - primary community code liason.</li>
              <li>
                Authored{' '}
                <a href="http://launchpad.net/pandora-build">pandora-build</a> -
                A set of simple and robust autotools macros.
              </li>
              <li>
                Authored{' '}
                <a href="http://launchpad.net/drizzle-interface">
                  drizzle-interface
                </a>{' '}
                - Bindings to libdrizzle.
              </li>
              <li>
                Authored{' '}
                <a href="http://launchpad.net/gearman-interface">
                  gearman-interface
                </a>{' '}
                - Bindings to libgearman.
              </li>
            </ul>
          </div>

          <div className="job">
            <p>MySQL</p>
            <p className="jobtitle">
              Senior Consultant: Remote (Seattle, WA / Stockholm, Sweden)
            </p>
            <p className="jobskillslist">
              MySQL, MySQL Cluster, Heartbeat, DRBD, Linux, C++, C#, Python,
              Java, PHP, Ruby
            </p>
            <ul>
              <li>
                Solutions Architect for top 10 web properties, telcos and gaming
                companies.
              </li>
              <li>Expert in High Availability and Clustering Solutions.</li>
              <li>
                Authored{' '}
                <a href="http://launchpad.net/ndb-bindings">NDB/Bindings</a> -
                Wrappers for MySQL Cluster NDB API in Java, Python, Ruby, C#,
                Perl and Lua.
              </li>
              <li>Member of the MySQL Debian Packaging team.</li>
              <li>
                Technical owner of MySQL/Linbit relationship related to DRBD.
              </li>
            </ul>
          </div>

          <h3 className="subsection">
            <span className="titletitle">Startups</span>
            <span className="titledate">1999-2011</span>
          </h3>

          <div className="job">
            <p>Import Brasileira, LLC</p>
            <p className="jobtitle">Founder: Seattle, WA</p>
            <ul>
              <li>Imported high quality Brazilian Cachaça.</li>
              <li>
                Won double-gold at the 2011 San Francisco International Spirits
                Competition.
              </li>
            </ul>
          </div>

          <div className="job">
            <p>StageFiles</p>
            <p className="jobtitle">Founder / Partner: Remote</p>
            <p className="jobskillslist">
              Plone, Python, mod_python, TurboGears, Debian/Ubuntu GNU/Linux
            </p>
            <ul>
              <li>Started, ran and managed company.</li>
              <li>
                Designed and developed a web-based portfolio system for
                Theatrical Design.
              </li>
            </ul>
          </div>

          <div className="job">
            <p>In August Productions</p>
            <p className="jobtitle">Founder, CEO</p>
            <p className="jobskillslist">
              Python, Zope, elisp, LaTeX, exim, ZODB, GTK, Glade, Cyrus, SASL,
              SquirrelMail, Apache, Debian GNU/Linux, Debian Packaging
            </p>
            <ul>
              <li>
                Started and managed small business concerns, including hiring,
                payroll and billing.
              </li>
              <li>
                Designed an extensible theatrical lighting control system.
              </li>
              <li>Wrote a typesetting system for theatre scripts.</li>
              <li>Co-developed automated email and web hosting system.</li>
              <em>Clients included:</em>

              <div className="job">
                <p>Washington Mutual: Seattle, WA</p>
                <p className="jobskillslist">
                  C#, Python, LaTeX, Python, Subversion, Apache2, Tomcat,
                  Solaris, Windows XP
                </p>
                <ul>
                  <li>
                    Developed LaTeX based reporting solution for Loan Prepayment
                    Modeling. Yes, someone paid me to work on LaTeX!
                  </li>
                  <li>
                    Provided system support including administration and system
                    design.
                  </li>
                </ul>
              </div>

              <div className="job">
                <p>Monster.com: Boston, MA</p>
                <p className="jobskillslist">
                  Zope, Python, DTML, C#, SQL Server, Perforce, Windows 2000
                </p>
                <ul>
                  <li>
                    Took over a Zope-based system for managing Job Content
                    written by external contractors who had left.
                  </li>
                  <li>
                    Single point of contact for Zope related issues for the
                    organization.
                  </li>
                </ul>
              </div>

              <div className="job">
                <p>Tahinis Mediterranean Bistro, Bar Harbor, ME: Remote</p>
                <p className="jobskillslist">
                  Plone, Python, Javascript, CSS, Debian GNU/Linux
                </p>
                <ul>
                  <li>Created an Online menu system for restaurants.</li>
                  <li>
                    Completely missed the now obvious opportunity to have become
                    Seamless or DoorDash.
                  </li>
                </ul>
              </div>

              <div className="job">
                <p>North Carolina State University: Remote</p>
                <p className="jobskillslist">
                  Zope, Python, mod-perl, Javascript, CSS, Apache, Debian
                  GNU/Linux, Solaris
                </p>
                <ul>
                  <li>
                    Created a system to help students write better lab reports.
                  </li>
                  <li>
                    Created a system to protect copywritten works used in course
                    materials.
                  </li>
                  <li>
                    Worked on web-based system to translate Western Characters
                    into Hindi.
                  </li>
                </ul>
              </div>

              <div className="job">
                <p>Cox Interactive Media: Atlanta, GA</p>
                <p className="jobskillslist">
                  Zope, Python, Oracle, PL/SQL, Java, Solaris
                </p>
                <ul>
                  <li>
                    Implemented a Content Management System in a combination of
                    Zope and Java Stored Procedures.
                  </li>
                  <li>
                    Marvelled at the fact that it took 3 solid days to install
                    Oracle. sssh, don't let Oracle hear me say that!
                  </li>
                </ul>
              </div>

              <div className="job">
                <p>Thingamy: Oslo, Norway</p>
                <p className="jobskillslist">Python</p>
                <ul>
                  <li>
                    Wrote a tool to migrate data from legacy database to ZODB.
                  </li>
                </ul>
              </div>

              <div className="job">
                <p>Alexander Consulting: London, England</p>
                <p className="jobskillslist">Zope</p>
                <ul>
                  <li>Provided solution assessment for proposed CRM System.</li>
                </ul>
              </div>

              <div className="job">
                <p>iuveno: Ingolstadt, Germany</p>
                <p className="jobskillslist">
                  Zope, Python, LDAP, IMAP, SuSE GNU/Linux, ZEO
                </p>
                <ul>
                  <li>
                    Worked on a web-based CRM system that interfaced Palm
                    Pilots.
                  </li>
                </ul>
              </div>

              <div className="job">
                <p>MTNI: Atlanta, GA</p>
                <p className="jobskillslist">Python</p>
                <ul>
                  <li>
                    Developed a Python library implementation of RFC 1861 -
                    SNPP.
                  </li>
                </ul>
              </div>
            </ul>

            <div className="job">
              <p>HRSmart</p>
              <p className="jobtitle">
                Senior System Administrator: Dallas, TX
              </p>
              <p className="jobskillslist">
                Apache, MySQL, Debian GNU/Linux, dirvish, Exim, Debian Packaging
              </p>
              <ul>
                <li>
                  Administered Debian GNU/Linux, Apache and MySQL servers.
                </li>
              </ul>
            </div>

            <div className="job">
              <p>Information Innovation</p>
              <p className="jobtitle">
                Information Artist / Lead Developer: Amsterdam, The Netherlands
              </p>
              <p className="jobskillslist">
                Perl, MySQL, Apache, Zope, Python, RedHat GNU/Linux, Solaris,
                MacOS X
              </p>
              <ul>
                <li>
                  Led development efforts for web-based strategic intelligence
                  product.
                </li>
                <li>
                  Designed and developed Internet news-feed reading and
                  processing system. 20 years before AI/ML. As a set of Perl
                  scripts. Because 1999.
                </li>
                <li>
                  Architected internal company systems for messaging and
                  Intranet.
                </li>
              </ul>
            </div>

            <h3 className="subsection">
              <span className="titletitle">Pre-Y2K Enterprise</span>

              <span className="titledate">1995-1999</span>
            </h3>

            <div className="job">
              <p>Branch Bank and Trust</p>
              <p className="jobtitle">
                Enterprise Management Systems Analyst: Wilson, NC
              </p>
              <p className="jobskillslist">
                Tivoli, Perl, ksh, AIX, Windows, Samba
              </p>
              <ul>
                <li>
                  Wrote and maintained Perl and shell scripts for Tivoli system
                  automation.
                </li>
                <li>
                  Used Samba to automate bootstrapping of Tivoli Endpoind
                  systems in remote bank branch locations on Windows
                  workstations from an AIX box. If you want a really fun story,
                  ask me about this one.
                </li>
              </ul>
            </div>

            <div className="job">
              <p>Best Consulting: Group Health Cooperative</p>
              <p className="jobtitle">Consultant: Seattle, WA</p>
              <p className="jobskillslist">
                Perl, ksh, Sybase, T-SQL, Stored Procedures, Solaris
              </p>
              <ul>
                <li>
                  Wrote code to update and manage Data Warehouse Common
                  Dimensions tables.
                </li>
                <li>
                  Used Perl Typeglobs that I could not possibly read today to
                  build a SQL generation system. One should never use Typeglobs,
                  except for the times when one should. Although I cannot
                  justify it today, I was able to successfully defend the use to
                  Mark-Jason Dominus on the first Perl Whirl Geek Cruise, so I
                  feel confident today that 1998 me wasn't completely off his
                  rocker.
                </li>
                <li>Understood enough Perl to know how to use Typeglobs.</li>
              </ul>
            </div>

            <div className="job">
              <p>EDS: Russell Stover Candies, Kansas City, MO</p>
              <p className="jobtitle">Information Analyst</p>
              <p className="jobskillslist">Perl, ksh, Sybase, T-SQL, AIX</p>
              <ul>
                <li>
                  Administered systems and performed DBA tasks supporting a 24x7
                  manufacturing and candy shipping environment.
                </li>
                <li>
                  Implemented a full GNU distribution in SMIT packages on to of
                  AIX.
                </li>
                <li>
                  Implemented a fully automated provisioning system for
                  installing and managing a fleet of AIX systems in remote
                  warehouse and factory locations. Today you might consider this
                  config managment.
                </li>
                <li>
                  Maintained an inherited barcode scanning system that involved
                  RF scanner guns running DOS, that telnetted to an AIX box
                  running a curses menu application that fed data into a 4G
                  database running on a Windows NT server. Remotely.
                </li>
                <li>
                  Was promised unlimited candy during interview, only to arrive
                  on first day to discover that the floor of geeks had eaten too
                  much candy and the policy had been revoked.
                </li>
              </ul>
            </div>

            <div className="job">
              <p>Fujitsu Network Communications, Dallas, TX</p>
              <p className="jobtitle">UNIX Admin/Support</p>
              <p className="jobskillslist">AIX, C, Framemaker SDK, ksh</p>
              <ul>
                <li>
                  Provided system and user support for Desktop AIX systems used
                  by the tech writing team.
                </li>
              </ul>
            </div>

            <div className="job">
              <p>IBM, Raleigh, NC</p>
              <p className="jobtitle">UNIX Admin/Support</p>
              <p className="jobskillslist">MVS, AIX, C, ksh</p>
              <ul>
                <li>
                  Provided system administration and AIX support for the TCP
                  Performance Testing on MVS.
                </li>
              </ul>
            </div>
          </div>
        </div>
      </Container>
    </>
  )
}
