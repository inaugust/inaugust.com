import './globals.css'

export const metadata = {
  title: 'Monty Taylor',
  description: "Monty Taylor's Tech Resume",
}

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body>{children}</body>
    </html>
  )
}
