import CssBaseline from '@mui/material/CssBaseline'
import Container from '@mui/material/Container'
import Box from '@mui/material/Box'
import styles from './page.module.css'

// Let's fix the styles later
import './resume.html/mordred.css'

export default function Home() {
  return (
    <>
      <CssBaseline />

      <Container maxWidth="xl">
        <div className="jumbotron">
          <h1>Monty Taylor</h1>
        </div>
        <Box>Things you may or may not care or not care about.</Box>
        <Box>
          It should be obvious, but I may be reworking what is going on here.
        </Box>
      </Container>
    </>
  )
}
