#!/bin/sh

gnome-terminal --geometry 68x24 -- ~/presentty/venv/bin/presentty zuul.rst &
geeqie -t images &

sleep 1

gnome-terminal --maximize --  ~/presentty/venv/bin/presentty-console zuul.rst &
