. display in 68x24
.. display in 88x24

.. transition:: dissolve
   :duration: 0.4

Update on OpenStackSDK / shade
==============================

Background
==========

Two Python OpenStack SDKs:

* openstacksdk
* shade

One support library:

* os-client-config

openstacksdk
============

* Object-Oriented presentation of OpenStack services and resources
* ``import openstack``
* Used by python-openstackclient for Neutron
* Used by heat for some Neutron
* Used by senlin, octavia-dashboard, masakari and bilean
* Unofficial, but with an official sounding name
* Exposes versions and services as they are in the API

.. code-block:: python

  # conn.image is openstack.image.v1._proxy.Proxy or
  # openstack.image.v2._proxy.Proxy depending on config (soon discovery)
  conn.image.images()
  # PUT /image
  conn.image.upload_image(name='foo')

shade
=====

* Abstraction layer covering up differences
* Originally extracted from Nodepool
* Used in Ansible OpenStack Modules (and now Salt)
* Official, but with an unofficial sounding name
* Excessively backwards compatible
* Resource/task oriented interface that hides service

.. code-block:: python

  # Calls glance or nova API as needed
  conn.list_images()
  # Uses v1, v2 PUT or v2 tasks as needed
  conn.create_image(name='foo')

os-client-config
================

* Library to handle API client account configuration
* Added support for ``clouds.yaml`` config files
* Used by shade, python-openstackclient, openstacksdk
* Run by OpenStackClient project team

Queens: The Great Merging
=========================

* Staffing on both projects less than desired, coupled with corporate cutbacks
* openstacksdk project adopted by shade team as a deliverable
* shade team renamed to OpenStackSDK team
* shade and os-client-config codebases merged in to openstacksdk repo
* shade.openstackcloud.OpenStackCloud -> openstack.connection.Connection
* os_client_config -> openstack.config

Queens: Major SDK refactors
===========================

* Proxy objects are now subclasses of keystoneauth1.adapter.Adapter
* Proxy objects are the objects that represent methods to call on a service
* Resource objects describe each remote resource
* Proxy objects attached to Connection
* Using official names and aliases from service-types-authority
* Proxy object for every service in service-types-authority
* Profile objects removed in favor of CloudRegion object from os-client-config
* Pagination supported by default for all OpenStack resources

Connection
==========

* Primary interface object
* Represents a connection to a region of a cloud

.. code-block:: python

  import openstack

  conn = openstack.connect(cloud='vexxhost')

Three Interfaces In One
=======================

* Abstraction layer from shade
* Object layer from SDK
* REST layer from keystoneauth

.. code-block:: python

  conn.list_images()  # list
  conn.image.images()  # generator
  conn.image.get('/image')  # requests.Response

A Note on Pagination
====================

* List methods (like ``conn.image.images``) are generators
* Transparently do pagination behind the scenes
* ``limit`` parameter is a requested batch size
* Server-side configured batch size may also be in effect
* If you want less then all results, stop iterating

What's Supported
================

* It's all driven by service-types-authority:
  https://service-types.openstack.org/
* Every official OpenStack service has at least REST interface
* Every official OpenStack service is welcome to add Proxy/Resource objects

Plugins/Drivers
===============

* No entrypoints-based drivers
* It's not openstacksdk's job to support non-OpenStack things
* Want to enable new projects to skip writing a python-*client library

.. code-block:: python

  class MyService(openstack.service_description.ServiceDescription):
      proxy_class = MyProxyClass
      service_type = 'awesome-service'

  conn.add_service(MyService())
  conn.awesome_service.create_awesome()

Facilities for Use by Services
==============================

* ``load_yaml_config`` and ``load_envvars`` flags
* ``openstack.connection.from_session`` - use existing authenticated Session

.. code-block:: python

   conn = openstack.connection.from_session(
      session=self.context.keystone_session,
      region_name=self._get_region_name(),
      app_name='heat',
      app_version=heat.version.version_info.version_string())

Compatibility Policy
====================

* shade's stance on backwards compat applies - once we release 1.0 of SDK
* sdk supports all existing OpenStack clouds
* Patches fixing interactions with a Diablo cloud would be accepted

Status
======

* os-client-config is now a thin shim around openstack.config
* service-types-authority aliases in keystoneauth (Just Released Friday)
* Version selection driven by config, patches up for version discovery

What's Next
===========

* Make Resource classes suitable for shade calls
* Start working on replacing use of python-*client in python-openstackclient
* Make shade a libraries thin compat layer
* Shift abstraction layer methods to use OO layer (currently use REST layer)

Help Wanted!
============

We still don't have enough people. Come play with us!

* freenode:#openstack-sdks
