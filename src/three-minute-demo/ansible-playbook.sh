#!/bin/bash

if [ -d ${HOME}/.config/openstack ] ; then
  CONFIG_DIR=${HOME}/.config/openstack
else
  CONFIG_DIR=/etc/openstack
fi

exec docker run -it --rm --privileged \
  -v${PWD}:/demo -v${CONFIG_DIR}:/etc/openstack \
  three-minute-demo $*
