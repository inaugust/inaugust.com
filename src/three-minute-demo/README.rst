3 Minute Demo of OpenStack Public Clouds
========================================

This is all the Ansible needed for the 3 minute public cloud demo.

It uses docker containers to avoid the need to install things on the local
computer.  The Dockerfile uses the base ``python`` docker image regardless
of other distro since it's just a container for installing ansible.

.. note::

  There is nothing docker-specific about the demo, it's just being used as
  a convenient way to bundle actions without polluting the user's computer.
  The playbooks themselves can be used directly for people who have the
  pre-reqs of ansible, shade and diskimage-builder installed or who do not
  mind installing them globally.

Building Docker Image
---------------------

Step one is building the docker image:

.. code-block:: bash

  docker build -t three-minute-demo .

A convenience script, ``build-docker-image.sh`` is provided that runs that
command.

Wrapper Script
--------------

Once the ``three-minute-demo`` image exists, the ``ansible-playbook.sh`` script
will user it to run ``ansible-playbook``. By default it will run the
``run.yaml`` playbook, but that can be overridden by passing parameters to the
script.

The ``ansible-playbook.sh`` script will also bind-mount the current directory
as the main work dir and ``~/.config/openstack`` into ``/etc/openstack`` in the
container so that ``clouds.yaml`` is available.

.. note::

  If ~/.config/openstack doesn't exist but /etc/openstack does,
  ansible-playbook.sh will mount /etc/openstack instead. If a different setup
  is desired, you can always run docker commands.

Building and Uploading Images
-----------------------------

The demo depends on base images existing. The ``prep.yaml`` playbook will
create the images and upload them to all of the cloud regions.

.. code-block:: bash

  ./ansible-playbook.sh prep.yaml

By default it will create a minimal ubuntu image. That can be changed by
setting ``distro`` on the ansible-playbook command line to something else, such
as ``fedora`` or ``centos``.

.. code-block:: bash

  ./ansible-playbook.sh -edistro=fedora prep.yaml

The Demo
--------

``ansible-playbook.sh`` defaults to running ``demo.yaml``, which will create a
server in each cloud region, install Apache on that server, write out a json
file for apache to serve, fetch the contents into a variable and print out the
FQDN.

If you're thinking "wow, that's not useful" - you're right! This isn't intended
to be a demo of doing useful things with Ansible. It's intended to be a demo of
getting servers on a set of cloud regions across multiple public clouds and
then doing something with them.

Once you can perform a single task on the remote server with Ansible, you can
do anything with Ansible you want to do.
