My website, blog posts and conference talks
===========================================

If you're interested in my presentations, you can go look at them here:
http://inaugust.com/talks

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

Building
--------

First, you will need `yarn`_.

The following commands will get you started:

.. code:: bash

    # This will install miscellaneous runtime dependencies.
    yarn install

    # This will create a local webhost, serving all of your presentations.
    # It will autodetect changes and refresh any applicable pages.
    yarn dev

    # This will build the content in the ``out`` directory
    yarn build

.. _yarn: https://yarnpkg.com/en/docs/install
